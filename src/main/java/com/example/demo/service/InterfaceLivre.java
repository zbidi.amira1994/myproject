package com.example.demo.service;

import java.util.List;
import com.example.demo.model.Livre;

public interface InterfaceLivre {
	List<Livre> getAllLivres();
	void addLivre(Livre livre);
	
}
