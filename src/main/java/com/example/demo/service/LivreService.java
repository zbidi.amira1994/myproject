package com.example.demo.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.example.demo.model.Livre;
import com.example.demo.model.Livres;

@Service
public class LivreService implements InterfaceLivre {
	private static Livres list = new Livres();
	static {
		list.getLivreList().add(new Livre(1, "978-1-60309-420-7", "Bottled"));
		list.getLivreList().add(new Livre(2, "978-1-60309-495-5", "Better Place"));
		list.getLivreList().add(new Livre(3, "978-1-60309-477-1", "Under-Earth"));
		list.getLivreList().add(new Livre(4, "978-1-60309-350-7", "Tim Ginger"));
		list.getLivreList().add(new Livre(5, "978-1-60309-478-8", "Bionic"));
	}

	@Override
	public List<Livre> getAllLivres() {
		return list.getLivreList();
	}

	@Override
	public void addLivre(Livre livre) {
		list.getLivreList().add(livre);
	}
}
