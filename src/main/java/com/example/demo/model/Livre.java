package com.example.demo.model;

public class Livre {

	private Integer id;
	private String isbn;
	private String name;

	public Livre(Integer id, String isbn, String name) {
		super();
		this.id = id;
		this.isbn = isbn;
		this.name = name;
	}

	public Livre() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
