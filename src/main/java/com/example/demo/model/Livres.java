package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

public class Livres {
	private List<Livre> livreList;

	public List<Livre> getLivreList() {
		if (livreList == null) {
			livreList = new ArrayList<>();
		}
		return livreList;
	}

	public void setLivreList(List<Livre> livreList) {
		this.livreList = livreList;
	}
}
