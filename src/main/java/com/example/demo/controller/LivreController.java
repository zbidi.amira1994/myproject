package com.example.demo.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.model.Livre;
import com.example.demo.service.LivreService;

@RestController
@RequestMapping("/livres")
public class LivreController {

	@Autowired
	LivreService livreService;

	@GetMapping("/list")
	public List<Livre> getLivreList() {
		return livreService.getAllLivres();
	}

	@PostMapping("/add")
	public ResponseEntity<Object> addEmployee(@RequestBody Livre livre) {
		Integer id = livreService.getAllLivres().size() + 1;
		livre.setId(id);
		livreService.addLivre(livre);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(livre.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

}
